# marco_utilities
My own utilities

This project is a collection of different concepts in Java for demonstrations purposes, ideally this should evolve into some utilities library.

* If it works for me, maybe works for you.
* This is a cutting edge project, as this writing Java 1.8+ is supported.
* Please help me to improve it.

Utilities covered
* Cryptography
* String manipulation
* Functional reactive programming.
