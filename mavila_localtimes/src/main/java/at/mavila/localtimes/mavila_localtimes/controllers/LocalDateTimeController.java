package at.mavila.localtimes.mavila_localtimes.controllers;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

/**
 * A sample component to show different operations that can be done in LocalTimeDate object.
 */
@RestController
@RequestMapping("localDateTimeController")
public class LocalDateTimeController implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6361070458373756370L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalDateTimeController.class);
    private static final String DATE_FORMAT = "dd.MM.yyyy_HH.mm.ss";
    private static DateTimeFormatter formatter;

    static {
        LOGGER.info("Static initialization to be done with the following format: {}", DATE_FORMAT);
        formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        LOGGER.info("Static initialization done: {}", formatter.toString());
    }

    /**
     * This method moves the date to the start of the month.
     *
     * @param date   we want to move
     * @param zoneId the zone id where we are located or default if empty
     * @return a formatted String with the date representation
     */
    @RequestMapping(value = "/convertToInitialMonthInDate", method = {RequestMethod.POST, RequestMethod.GET})
    public String convertToInitialMonthInDate(@RequestParam(value = "date", required = false)
                                              @DateTimeFormat(pattern = "dd.MM.yyyy") final Date date,

                                              @RequestParam(value = "zoneId", required = false) final String zoneId
    ) {
        //Date as original passed in the parameter
        final LocalDateTime originalDateTime = LocalDateTime.ofInstant(
                Objects.isNull(date) ? new Date().toInstant() : date.toInstant(), getTimeZone(zoneId).toZoneId());

        //Gets the current month of such date
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        final LocalDateTime inTheMonthDateTime = originalDateTime.withDayOfMonth(1);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        //Formats both dates
        final String inTheMonthDate = inTheMonthDateTime.format(formatter);
        final String originalDate = originalDateTime.format(formatter);

        //Calculates the days elapsed between these two dates.
        final int dayElapsedSinceStartOfTheMonth =
                originalDateTime.getDayOfMonth() - inTheMonthDateTime.getDayOfMonth();

        //Prints it.
        return String.format("%s -> %s (days elapsed: %s)",
                inTheMonthDate,
                originalDate,
                Integer.toString(dayElapsedSinceStartOfTheMonth));
    }

    private static TimeZone getTimeZone(String zoneId) {

        if (StringUtils.isNotBlank(zoneId)) {
            return TimeZone.getTimeZone(zoneId);
        }

        final TimeZone timeZoneDefault = TimeZone.getDefault();
        LOGGER.warn("Blank zone id, returning the time zone where the JVM is running: {}", timeZoneDefault.getID());
        return timeZoneDefault;
    }

}
