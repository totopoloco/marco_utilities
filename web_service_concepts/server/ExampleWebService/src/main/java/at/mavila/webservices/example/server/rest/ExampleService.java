/**
 * 
 */
package at.mavila.webservices.example.server.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.Validate;
import org.json.JSONObject;

/**
 * @author mavila
 *
 */
@Path("/hello")
public class ExampleService {

	@SuppressWarnings("static-method")
	@GET
	@Path("/{param}")
	@Produces("application/json")
	public Response getMsg(@PathParam("param") final String msg) {
		Validate.notNull(msg, "Message must not be null");
		return Response.status(200).entity((new JSONObject().put("Message", msg)).toString()).build();
	}

}
