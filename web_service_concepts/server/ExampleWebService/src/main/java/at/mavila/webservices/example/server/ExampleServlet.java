package at.mavila.webservices.example.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class ExampleServlet
 */
public class ExampleServlet extends HttpServlet {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = -64181279392687002L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ExampleServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExampleServlet() {
		LOGGER.info("New servlet");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.debug("doPost");
		PrintWriter out = null;
		try {
			// Set response content type
			response.setContentType("text/html");

			// Actual logic goes here.
			out = response.getWriter();
			out.println("<html><h1>Hi there!</h1></html>");
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}  //finally
	}

}
