package at.mavila.exercises.duplicates.lists;

import at.mavila.exercises.duplicates.pojos.Person;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Collections.nCopies;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public final class DuplicatePersonFilter {

    private DuplicatePersonFilter() {
        //No instances of this class
    }

    public static List<Person> removeDuplicates(final List<Person> personList) {

        return personList
                .stream()
                .filter(duplicateByKey(Person::getId))
                .filter(duplicateByKey(Person::getFirstName))
                .collect(toList());

    }

    private static <T> Predicate<T> duplicateByKey(final Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> isNull(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE));

    }


    public static List<Person> extractDuplicates(List<Person> personList) {
        return getDuplicatesMap(personList).values().stream()
                .filter(duplicates -> duplicates.size() > 1)
                .flatMap(Collection::stream)
                .collect(toList());
    }

    public static List<Person> extractDuplicatesWithIdentityCountingV1(final List<Person> personList){

        List<Person> duplicates = personList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(n -> n.getValue() > 1)
                .map(n -> n.getKey())
                .collect(toList());

        return duplicates;

    }

    public static List<Person> extractDuplicatesWithIdentityCountingV2(final List<Person> personList) {

        List<Person> duplicates = personList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(n -> n.getValue() > 1)
                .flatMap(n -> nCopies(n.getValue().intValue(), n.getKey()).stream())
                .collect(toList());

        return duplicates;

    }

    private static Map<String, List<Person>> getDuplicatesMap(List<Person> personList) {
        return personList.stream().collect(groupingBy(DuplicatePersonFilter::uniqueAttributes));
    }

    private static String uniqueAttributes(Person person){

        if(Objects.isNull(person)){
            return StringUtils.EMPTY;
        }

        return (person.getId()) + (person.getFirstName()) ;
    }


}
