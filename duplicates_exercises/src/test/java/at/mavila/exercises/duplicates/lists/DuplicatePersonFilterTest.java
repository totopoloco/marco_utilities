package at.mavila.exercises.duplicates.lists;

import at.mavila.exercises.duplicates.pojos.Person;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DuplicatePersonFilterTest {

    private static final Logger LOGGER = LogManager.getLogger(DuplicatePersonFilterTest.class);

    static {
        BasicConfigurator.configure();
    }

    @Test
    public void testRemoveDuplicatesMethod(){
        Person alex = new Person.Builder().id(1L).firstName("alex").secondName("salgado").build();
        Person lolita = new Person.Builder().id(2L).firstName("lolita").secondName("llanero").build();
        Person elpidio = new Person.Builder().id(3L).firstName("elpidio").secondName("ramirez").build();
        Person romualdo = new Person.Builder().id(4L).firstName("romualdo").secondName("gomez").build();
        Person otroRomualdo = new Person.Builder().id(4L).firstName("romualdo").secondName("perez").build();


        List<Person> personList = new ArrayList<>();

        personList.add(alex);
        personList.add(lolita);
        personList.add(elpidio);
        personList.add(romualdo);
        personList.add(otroRomualdo);

        final List<Person> cleaneList = DuplicatePersonFilter.removeDuplicates(personList);
        assertThat(cleaneList).containsExactly(alex, lolita, elpidio, romualdo);

    }

    @Test
    public void testExtractDuplicatesMethod(){
        Person alex = new Person.Builder().id(1L).firstName("alex").secondName("salgado").build();
        Person lolita = new Person.Builder().id(2L).firstName("lolita").secondName("llanero").build();
        Person elpidio = new Person.Builder().id(3L).firstName("elpidio").secondName("ramirez").build();
        Person romualdo = new Person.Builder().id(4L).firstName("romualdo").secondName("gomez").build();
        Person otroRomualdo = new Person.Builder().id(4L).firstName("romualdo").secondName("perez").build();


        List<Person> personList = new ArrayList<>();

        personList.add(alex);
        personList.add(lolita);
        personList.add(elpidio);
        personList.add(romualdo);
        personList.add(otroRomualdo);

        final List<Person> duplicates = DuplicatePersonFilter.extractDuplicates(personList);
        assertThat(duplicates).containsExactly(romualdo, otroRomualdo);

    }

    @Test
    public void testExtractDuplicatesV1(){
        Person alex = new Person.Builder().id(1L).firstName("alex").secondName("salgado").build();
        Person lolita = new Person.Builder().id(2L).firstName("lolita").secondName("llanero").build();
        Person elpidio = new Person.Builder().id(3L).firstName("elpidio").secondName("ramirez").build();
        Person romualdo0 = new Person.Builder().id(4L).firstName("romualdo").secondName("gomez").build();
        Person romualdo1 = new Person.Builder().id(4L).firstName("romualdo").secondName("gomez").build();
        Person pedro0 = new Person.Builder().id(5L).firstName("pedro").secondName("perez").build();
        Person pedro1 = new Person.Builder().id(5L).firstName("pedro").secondName("perez").build();
        Person pedro2 = new Person.Builder().id(5L).firstName("pedro").secondName("perez").build();


        List<Person> personList = new ArrayList<>();

        personList.add(alex);
        personList.add(lolita);
        personList.add(elpidio);
        personList.add(romualdo0);
        personList.add(romualdo1);
        personList.add(pedro0);
        personList.add(pedro1);
        personList.add(pedro2);

        final List<Person> duplicates = DuplicatePersonFilter.extractDuplicatesWithIdentityCountingV1(personList);
        assertThat(duplicates).containsExactly(romualdo0, pedro0);

    }

    @Test
    public void testExtractDuplicatesV2(){
        Person alex = new Person.Builder().id(1L).firstName("alex").secondName("salgado").build();
        Person lolita = new Person.Builder().id(2L).firstName("lolita").secondName("llanero").build();
        Person elpidio = new Person.Builder().id(3L).firstName("elpidio").secondName("ramirez").build();
        Person romualdo0 = new Person.Builder().id(4L).firstName("romualdo").secondName("gomez").build();
        Person romualdo1 = new Person.Builder().id(4L).firstName("romualdo").secondName("gomez").build();
        Person pedro0 = new Person.Builder().id(5L).firstName("pedro").secondName("perez").build();
        Person pedro1 = new Person.Builder().id(5L).firstName("pedro").secondName("perez").build();
        Person pedro2 = new Person.Builder().id(5L).firstName("pedro").secondName("perez").build();


        List<Person> personList = new ArrayList<>();

        personList.add(alex);
        personList.add(lolita);
        personList.add(elpidio);
        personList.add(romualdo0);
        personList.add(romualdo1);
        personList.add(pedro0);
        personList.add(pedro1);
        personList.add(pedro2);

        final List<Person> duplicates = DuplicatePersonFilter.extractDuplicatesWithIdentityCountingV2(personList);
        assertThat(duplicates).containsExactly(romualdo0, romualdo1, pedro0, pedro1, pedro2);

    }

}