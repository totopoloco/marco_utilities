package at.mavila.exercises.duplicates.pojos;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class PersonTest {

    @Test
    public void whenEmptyPersonThenSamePerson(){
        Person person0 = new Person.Builder().id(1L).build();
        Person person1 = new Person.Builder().id(1L).build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(0);

    }

    @Test(expected = NullPointerException.class)
    public void whenPerson0HasIdThenNotTheSame(){
        Person person0 = new Person.Builder().id(2L).build();
        Person person1 = new Person.Builder().build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(1);

    }


    @Test(expected = NullPointerException.class)
    public void whenPerson0HasNegativeIdThenNotTheSame(){
        Person person0 = new Person.Builder().id(78744L).build();
        Person person1 = new Person.Builder().build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(1);

    }

    @Test
    public void whenPersonsHavesIdsThenNotTheSame(){
        Person person0 = new Person.Builder().id(78744L).build();
        Person person1 = new Person.Builder().id(99999L).build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(-1);

    }


    @Test
    public void whenPersonsHaveIdAndNamesThenNotTheSame(){
        Person person0 = new Person.Builder().id(78744L).firstName("juan").build();
        Person person1 = new Person.Builder().id(99999L).firstName("pedro").build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(-1);

    }


    @Test
    public void whenPersonsHaveOnlyNamesThenNotTheSame(){
        Person person0 = new Person.Builder().id(1L).firstName("juan").build();
        Person person1 = new Person.Builder().id(1L).firstName("pedro").build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(-1);

    }

    @Test
    public void whenPersonsHaveSlightNameDifferenceThenNotTheSame(){
        Person person0 = new Person.Builder().id(1L).firstName("a").build();
        Person person1 = new Person.Builder().id(1L).firstName("b").build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(-1);

    }

    @Test
    public void whenPersonsHaveSlightName2DifferenceThenNotTheSame(){
        Person person0 = new Person.Builder().id(1L).firstName("b").build();
        Person person1 = new Person.Builder().id(1L).firstName("a").build();

        final int i = person0.compareTo(person1);
        assertThat(i).isEqualTo(1);

    }

}