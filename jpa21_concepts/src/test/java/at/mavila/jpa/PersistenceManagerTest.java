package at.mavila.jpa;

import at.mavila.jpa.pojos.Address;
import at.mavila.jpa.pojos.User;
import at.mavila.jpa.utils.PersistenceManager;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by marcotulioavilaceron on 24/10/15.
 */
public class PersistenceManagerTest {

        private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceManagerTest.class);
        private static EntityManager ENTITY_MANAGER;

        @BeforeClass
        public void begin(){
                ENTITY_MANAGER.getTransaction().begin();
                LOGGER.info("Began transaction.");
        }

        @BeforeTest
        public void init() {
                BasicConfigurator.configure();
                ENTITY_MANAGER = PersistenceManager.INSTANCE.getEntityManager();
                LOGGER.info("Logging started and got a new entity manager.");
        }

        @AfterTest
        public void close() {
                ENTITY_MANAGER.close();
                PersistenceManager.INSTANCE.close();
                LOGGER.info("Closed the entity manager.");

        }

        @AfterClass
        public void commit() {
                ENTITY_MANAGER.getTransaction().commit();
                LOGGER.info("Commited the transaction.");
        }

        @Test
        public void testInsertNoUsers() {

                for (int i = 0; i < 10; i++) {
                        int index = i + 1;
                        final Address address = new Address();

                        address.setStreet("Poribagh nr 66" + index);
                        address.setCity("Dhaka " + index);
                        address.setState("X State " + index);
                        address.setCountry("Bangladesh " + index);
                        address.setPostcode("1000 " + index);

                        ENTITY_MANAGER.persist(address);

                        LOGGER.debug("Newly inserted entity's id is: {}",
                                new Object[] { address.getAddressId().toString() });

                        assertThat(address.getCity()).isEqualTo("Dhaka " + index);
                        assertThat(address.getStreet()).isEqualTo("Poribagh nr 66" + index);
                        assertThat(address.getState()).isEqualTo("X State " + index);
                        assertThat(address.getPostcode()).isEqualTo("1000 " + index);
                        assertThat(address.getAddressId()).isGreaterThan(Long.valueOf(0));
                }

        }

        @Test
        public void testInsertWithUsers() {

                for (int i = 0; i < 10; i++) {
                        int index = i + 1;
                        final Address address = new Address();

                        address.setStreet("Poribagh nr 66" + index);
                        address.setCity("Dhaka " + index);
                        address.setState("X State " + index);
                        address.setCountry("Bangladesh " + index);
                        address.setPostcode("1000 " + index);

                        ENTITY_MANAGER.persist(address);

                        LOGGER.debug("Newly inserted entity's id is: {}",
                                new Object[] { address.getAddressId().toString() });

                        assertThat(address.getCity()).isEqualTo("Dhaka " + index);
                        assertThat(address.getStreet()).isEqualTo("Poribagh nr 66" + index);
                        assertThat(address.getState()).isEqualTo("X State " + index);
                        assertThat(address.getPostcode()).isEqualTo("1000 " + index);
                        assertThat(address.getAddressId()).isGreaterThan(Long.valueOf(0));
                }

                //Retrieve the first address
                Query query = ENTITY_MANAGER.createQuery(" from Address a where a.addressId = :id")
                        .setParameter("id", Long.valueOf(1));
                List<Address> resultList = query.getResultList();

                final Address address = resultList.get(0);
                final User user0 = new User();
                user0.setName("Name0");
                user0.setAddress(address);



                final Address addressWithOneUser = new Address();
                addressWithOneUser.setAddressId(address.getAddressId());
                addressWithOneUser.setStreet(address.getStreet());


                addressWithOneUser.setCity(address.getCity());

                addressWithOneUser.setState(address.getState());

                addressWithOneUser.setCountry(address.getCountry());

                addressWithOneUser.setPostcode(address.getPostcode());
                //adressWithOneUser.setUsers(address.getUsers());
                addressWithOneUser.addUser(user0);

                ENTITY_MANAGER.persist(user0);
                //ENTITY_MANAGER.merge(adressWithOneUser);

                Query query1 = ENTITY_MANAGER.createQuery(" from Address a where a.addressId = :id")
                        .setParameter("id", Long.valueOf(1));
                List<Address> resultList1 = query1.getResultList();

                LOGGER.debug("Newly inserted entity's id is: {}", new Object[] { user0.toString() });

                assertThat(resultList1.get(0).getStreet()).contains(addressWithOneUser.getStreet());
                assertThat(resultList1.get(0).getUsers()).contains(user0);

        }

}