package at.mavila.jpa.querybuilder;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;


import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 200000313 on 04.01.2016.
 */
public class QueryBuilderTest {

    private static final Logger LOGGER = Logger.getLogger(QueryBuilderTest.class);

    @Test
    public void testCreateQuery() throws Exception {

        final String res = QueryBuilder.createQuery();
        LOGGER.debug("RES: " +res);
        assertThat(res).isNotEmpty();

    }
}