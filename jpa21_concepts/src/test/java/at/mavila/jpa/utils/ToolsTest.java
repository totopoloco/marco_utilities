package at.mavila.jpa.utils;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 200000313 on 18.12.2015.
 */
public class ToolsTest {

    @Test
    public void testGetSafeDouble() throws Exception {

        final double safeDoublepre0 = Tools.getSafeDouble("12334,78");  //Clean it from the commas
        assertThat(safeDoublepre0).isEqualTo(12334.78d);

        final double safeDouble00 = Tools.getSafeDouble("12334.78");  //Clean it from the commas
        assertThat(safeDouble00).isEqualTo(12334.78d);

        final double safeDouble2 = Tools.getSafeDouble("12.334,78");  //Clean it from the commas
        assertThat(safeDouble2).isEqualTo(12334.78d);


        final double safeDouble1 = Tools.getSafeDouble("12,334.78");  //Accept only the last two dots
        assertThat(safeDouble1).isEqualTo(12334.78d);

        final double safeDouble0 = Tools.getSafeDouble("12.334.78");  //Accept only the last two dots
        assertThat(safeDouble0).isEqualTo(0);//).isEqualTo(12334.78d));

        final double safeDouble3 = Tools.getSafeDouble("1fdssd2vcxvxc33gfdgfd4,78");  //Accept only the last two dots
        assertThat(safeDouble3).isEqualTo(0);

        final double safeDouble4 = Tools.getSafeDouble("12.334,78");  //Clean it from the commas
        assertThat(safeDouble4).isEqualTo(12334.78d);


        final double safeDouble5 = Tools.getSafeDouble("23,5453453,12,334,78");  //Clean it from the commas
        assertThat(safeDouble5).isEqualTo(0);

        final double safeDouble6 = Tools.getSafeDouble("12334");  //Clean it from the commas
        assertThat(safeDouble6).isEqualTo(12334d);


        final double safeDouble7 = Tools.getSafeDouble("abcdekjj");  //Clean it from the commas
        assertThat(safeDouble7).isEqualTo(0d);

        final double safeDouble8 = Tools.getSafeDouble("1.07");  //Clean it from the commas
        assertThat(safeDouble8).isEqualTo(1.07);

        final double safeDouble9 = Tools.getSafeDouble("1,07");  //Clean it from the commas
        assertThat(safeDouble9).isEqualTo(1.07);


        final double safeDouble10 = Tools.getSafeDouble("1,0fsdf7");  //Clean it from the commas
        assertThat(safeDouble10).isEqualTo(0);

        final double safeDouble11 = Tools.getSafeDouble(".1");  //Clean it from the commas
        assertThat(safeDouble11).isEqualTo(0.1);

        final double safeDouble12 = Tools.getSafeDouble("-99");  //Clean it from the commas
        assertThat(safeDouble12).isEqualTo(Double.valueOf("-99"));

        final double safeDouble13 = Tools.getSafeDouble(",1");  //Clean it from the commas
        assertThat(safeDouble13).isEqualTo(0.1);

        final double safeDouble14 = Tools.getSafeDouble("-99.75745");  //Clean it from the commas
        assertThat(safeDouble14).isEqualTo(Double.valueOf("-99.75745"));


        final double safeDouble15 = Tools.getSafeDouble("-23,5453453,12,334,78");  //Parseable good number.
        assertThat(safeDouble15).isEqualTo(Double.valueOf("0"));


        final double safeDouble16 = Tools.getSafeDouble("-.75745");  //Clean it from the commas
        assertThat(safeDouble16).isEqualTo(Double.valueOf("-0.75745"));


        final double safeDouble17 = Tools.getSafeDouble(" -0.5 ");  //Clean it from the commas
        assertThat(safeDouble17).isEqualTo(Double.valueOf("-0.5"));


        final double safeDouble18 = Tools.getSafeDouble(" 0.5 ");  //Clean it from the commas
        assertThat(safeDouble18).isEqualTo(Double.valueOf("0.5"));


        final double safeDouble19 = Tools.getSafeDouble("83883,,83883");  //No double ,,
        assertThat(safeDouble19).isEqualTo(Double.valueOf("0"));


        final double safeDouble20 = Tools.getSafeDouble("834233..834324883");  //Clean it from the commas
        assertThat(safeDouble20).isEqualTo(Double.valueOf("0"));


        final double safeDouble21 = Tools.getSafeDouble("834.2338343.24883..");  //Clean it from the commas
        assertThat(safeDouble21).isEqualTo(Double.valueOf("0"));


        final double safeDouble22 = Tools.getSafeDouble("-0.75745");  //Clean it from the commas
        assertThat(safeDouble22).isEqualTo(Double.valueOf("-0.75745"));


        final double safeDouble23 = Tools.getSafeDouble("14");  //Clean it from the commas
        assertThat(safeDouble23).isEqualTo(Double.valueOf("14"));


        final double safeDouble24 = Tools.getSafeDouble("--14");  //Clean it from the commas
        assertThat(safeDouble24).isEqualTo(Double.valueOf("0"));


        final double safeDouble25 = Tools.getSafeDouble("       -14                     ");  //Clean it from the commas
        assertThat(safeDouble25).isEqualTo(Double.valueOf("-14"));


        final double safeDouble26 = Tools.getSafeDouble("12,334,543,553,332.44899");  //Clean it from the commas
        assertThat(safeDouble26).isEqualTo(Double.valueOf("12334543553332.44899"));


        System.out.print("---------");

    }
}