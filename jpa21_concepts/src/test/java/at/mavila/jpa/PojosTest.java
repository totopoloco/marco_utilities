package at.mavila.jpa;

import at.mavila.jpa.pojos.Address;
import at.mavila.jpa.pojos.User;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.filters.FilterPackageInfo;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by marcotulioavilaceron on 25/10/15.
 */
public class PojosTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PojosTest.class);

    // Configured for expectation, so we know when a class gets added or removed.
    private static final int EXPECTED_CLASS_COUNT = 8;

    // The package to test
    private static final String POJO_PACKAGE = "at.mavila.jpa.pojos";

    @BeforeTest
    public void init(){
        BasicConfigurator.configure();
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Address.class).suppress(Warning.STRICT_INHERITANCE).verify();
        EqualsVerifier.forClass(User.class).suppress(Warning.STRICT_INHERITANCE).verify();
    }

    @Test
    public void ensureExpectedPojoCount() {
        List<PojoClass> pojoClasses = PojoClassFactory.getPojoClasses(POJO_PACKAGE,
                new FilterPackageInfo());


        assertThat(pojoClasses.size()).isEqualTo(EXPECTED_CLASS_COUNT);
        LOGGER.info("Done");
    }

    @Test
    public void testPojoStructureAndBehavior() {
        Validator validator = ValidatorBuilder.create()
                // Add Rules to validate structure for POJO_PACKAGE
                // See com.openpojo.validation.rule.impl for more ...
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                        // Add Testers to validate behaviour for POJO_PACKAGE
                        // See com.openpojo.validation.test.impl for more ...
                .with(new SetterTester())
                .with(new GetterTester())
                .build();

        validator.validate(POJO_PACKAGE, new FilterPackageInfo());
        LOGGER.info("Done");
    }

}