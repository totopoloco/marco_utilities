package at.mavila.jpa.operations;

import at.mavila.jpa.pojos.QSearchName;
import at.mavila.jpa.pojos.SearchName;
import com.mysema.query.types.expr.BooleanExpression;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StopWatch;

import java.util.Iterator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring-config.xml" })
@EnableJpaRepositories
@EnableTransactionManagement
public class SearchRepositoryTest {

        private static final Logger LOGGER = LoggerFactory.getLogger(SearchRepositoryTest.class);

        @Autowired
        SearchNameRepository searchNameRepository;


        @BeforeClass
        public static void setUp() throws Exception {
                BasicConfigurator.configure();
        }


        @Test
        public void testRetrieve() {

                StopWatch stopWatch = new StopWatch("testRetrieve");
                stopWatch.start();
                final BooleanExpression booleanExpression = QSearchName.user.zuname.startsWith("LOPEZ").
                        and(QSearchName.user.vorname.startsWith("ADAMSON")).
                        and(QSearchName.user.gfCreatedBranch.eq(Long.valueOf("99088745")));
                final Iterable<SearchName> searches = this.searchNameRepository.findAll(booleanExpression, createPageRequestDesc());

                final Iterator<SearchName> searchNameIterator = searches.iterator();

                while(searchNameIterator.hasNext()){
                        SearchName searchName = searchNameIterator.next();
                        LOGGER.info(searchName.toString());
                }

                stopWatch.stop();
                 LOGGER.info("Done...... in {}",stopWatch.prettyPrint());

        }


        private Pageable createPageRequestDesc() {
                return new PageRequest(0,
                        3,
                        new Sort(Sort.Direction.DESC, "zuname"));

        }

}