package at.mavila.jpa.operations;

import at.mavila.jpa.pojos.Search;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by 200000313 on 22.12.2015.
 */
public interface SearchRepository extends CrudRepository<Search, Long>, QueryDslPredicateExecutor<Search> {

}
