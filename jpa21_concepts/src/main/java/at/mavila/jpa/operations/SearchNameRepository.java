package at.mavila.jpa.operations;

import at.mavila.jpa.pojos.SearchName;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by 200000313 on 22.12.2015.
 */
public interface SearchNameRepository extends CrudRepository<SearchName, Long>, QueryDslPredicateExecutor<SearchName> {
}
