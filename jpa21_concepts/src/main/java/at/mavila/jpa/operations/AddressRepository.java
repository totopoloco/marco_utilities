package at.mavila.jpa.operations;

import at.mavila.jpa.pojos.Address;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by 200000313 on 10.12.2015.
 */
public interface AddressRepository extends CrudRepository<Address, Long> {
}
