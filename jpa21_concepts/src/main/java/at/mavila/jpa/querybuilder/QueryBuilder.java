package at.mavila.jpa.querybuilder;

import com.healthmarketscience.sqlbuilder.*;
import com.healthmarketscience.sqlbuilder.dbspec.basic.DbColumn;
import com.healthmarketscience.sqlbuilder.dbspec.basic.DbSchema;
import com.healthmarketscience.sqlbuilder.dbspec.basic.DbSpec;
import com.healthmarketscience.sqlbuilder.dbspec.basic.DbTable;

import java.util.HashMap;
import java.util.Map;

import static at.mavila.jpa.querybuilder.QueryBuilderConstants.*;

/**
 * Created by 200000313 on 04.01.2016.
 */
public final class QueryBuilder {

    private static final Map<String, DbTable> TABLES = new HashMap<String, DbTable>();
    private static final Map<String, DbColumn> COLUMNS = new HashMap<String, DbColumn>();
    private static final Map<String, DbColumn> COLUMNS_WITH_NAME = new HashMap<String, DbColumn>();


    static{
        createTables();
        createColumnsSearch();
        createColumnsWithName();
    }

    private QueryBuilder() {
        //No instances
    }

    private static void createColumnsWithName() {
        final DbTable dbTable = TABLES.get(MER_V_GF_SEARCH_WITH_NAME);
        COLUMNS_WITH_NAME.putAll(COLUMNS);
        COLUMNS_WITH_NAME.put(OBJEKT, dbTable.addColumn(OBJEKT));
    }

    private static void createTables() {
        final DbSpec dbSpec = new DbSpec();
        final DbSchema dbSchema = dbSpec.addSchema(DOLPHIN);
        final DbTable dbTableSearchWithName = dbSchema.addTable(MER_V_GF_SEARCH_WITH_NAME);
        final DbTable dbTableSearch = dbSchema.addTable(MER_V_GF_SEARCH);

        TABLES.put(MER_V_GF_SEARCH_WITH_NAME, dbTableSearchWithName);
        TABLES.put(MER_V_GF_SEARCH, dbTableSearch);
    }

    private static void createColumnsSearch() {
        final DbTable dbTable = TABLES.get(MER_V_GF_SEARCH);
        COLUMNS.put(ID, dbTable.addColumn(ID));
        COLUMNS.put(FINANZ_ART, dbTable.addColumn(FINANZ_ART));
        COLUMNS.put(GS_KDNR, dbTable.addColumn(GS_KDNR));
        COLUMNS.put(LVORNAME, dbTable.addColumn(LVORNAME));
        COLUMNS.put(LZUNAME, dbTable.addColumn(LZUNAME));
        COLUMNS.put(ZUNAME, dbTable.addColumn(ZUNAME));
        COLUMNS.put(VORNAME, dbTable.addColumn(VORNAME));
        COLUMNS.put(KUNDE, dbTable.addColumn(KUNDE));
        COLUMNS.put(KDNR, dbTable.addColumn(KDNR));
        COLUMNS.put(GF_NUMMER, dbTable.addColumn(GF_NUMMER));
        COLUMNS.put(ALT_VTNR, dbTable.addColumn(ALT_VTNR));
        COLUMNS.put(ENTSCHEIDUNGS_KZ, dbTable.addColumn(ENTSCHEIDUNGS_KZ));
        COLUMNS.put(SUCH_BEGRIFF, dbTable.addColumn(SUCH_BEGRIFF));
        COLUMNS.put(KREDITBETRAG, dbTable.addColumn(KREDITBETRAG));
        COLUMNS.put(LAUFZEIT, dbTable.addColumn(LAUFZEIT));
        COLUMNS.put(ACC_TIME, dbTable.addColumn(ACC_TIME));
        COLUMNS.put(PERS_NR, dbTable.addColumn(PERS_NR));
        COLUMNS.put(GF_CREATED_BRANCH, dbTable.addColumn(GF_CREATED_BRANCH));
        COLUMNS.put(FINANZIERUNGS_KZ, dbTable.addColumn(FINANZIERUNGS_KZ));
        COLUMNS.put(ABDECK_ENTHALTEN, dbTable.addColumn(ABDECK_ENTHALTEN));
        COLUMNS.put(ABDECK_BETRAG, dbTable.addColumn(ABDECK_BETRAG));
        COLUMNS.put(KREDITART, dbTable.addColumn(KREDITART));
        COLUMNS.put(PRODCODEPOL_60, dbTable.addColumn(PRODCODEPOL_60));
        COLUMNS.put(GF_STATUS, dbTable.addColumn(GF_STATUS));
        COLUMNS.put(RATE, dbTable.addColumn(RATE));
        COLUMNS.put(ROLLE, dbTable.addColumn(ROLLE));
        COLUMNS.put(SACHBEARBEITER, dbTable.addColumn(SACHBEARBEITER));
        COLUMNS.put(WEBORDER_ID, dbTable.addColumn(WEBORDER_ID));

    }

    public static String createQuery() {
        final SelectQuery selectQuery = new SelectQuery();
        selectQuery.addColumns(COLUMNS.get(ID),COLUMNS.get(GF_STATUS));
        selectQuery.addCondition(ComboCondition.or(new BinaryCondition(BinaryCondition.Op.EQUAL_TO,COLUMNS.get("RATE"),883.33d),
                new BinaryCondition(BinaryCondition.Op.EQUAL_TO, COLUMNS.get(KREDITART), "L"))
                );
        return selectQuery.validate().toString();
    }
}
