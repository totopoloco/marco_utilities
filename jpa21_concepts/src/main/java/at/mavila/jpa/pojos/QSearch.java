package at.mavila.jpa.pojos;

import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.PathMetadataFactory;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.PathInits;
import com.mysema.query.types.path.StringPath;

/**
 * Created by 200000313 on 22.12.2015.
 */


public class QSearch extends EntityPathBase<Search> {

        private static final PathInits INITS = PathInits.DIRECT2;



        public static final QSearch user = new QSearch("search");
        public final StringPath zuname = createString("zuname");
        public final StringPath vorname = createString("vorname");

        public QSearch(Class<? extends Search> type, PathMetadata<?> metadata) {
                super(type, metadata);
        }

        private QSearch(String variable) {
                this(Search.class, PathMetadataFactory.forVariable(variable), INITS);
        }

        private QSearch(Class<? extends Search> type, PathMetadata<?> metadata, PathInits inits) {
                super(type, metadata, inits);
        }


        public static QSearch getUser() {
                return user;
        }

        public StringPath getVorname() {
                return vorname;
        }

        public StringPath getZuname() {
                return zuname;
        }
}
