package at.mavila.jpa.pojos;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * CoreSearch.
 */
@MappedSuperclass
public class CoreSearch implements Serializable {
        /**
         * serialVersionUID
         */
        private static final long serialVersionUID = 5771635555838949073L;

        @Id
        @Column(name = "ID")
        protected Long id;

        ///---------------------------------
        @Column(name = "FINANZ_ART")
        protected String finanzArt;

        @Column(name = "GS_KDNR")
        protected Long gsKdnr;

        @Column(name = "LVORNAME")
        protected String lvorname;

        @Column(name = "LZUNAME")
        protected String lzuname;

        @Column(name = "ZUNAME")
        protected String zuname;

        @Column(name = "VORNAME")
        protected String vorname;

        @Column(name = "KUNDE")
        protected Long kunde;

        @Column(name = "KDNR")
        protected Long kdnr;

        @Column(name = "GF_NUMMER")
        protected String gfNummer;

        @Column(name = "ALT_VTNR")
        protected String altVtnr;

        @Column(name = "ENTSCHEIDUNGS_KZ")
        protected String entscheidungsKz;

        @Column(name = "SUCH_BEGRIFF")
        protected String suchBegriff;

        @Column(name = "KREDITBETRAG")
        protected Double kreditBetrag;

        @Column(name = "LAUFZEIT")
        protected Long laufzeit;

        @Column(name = "ACC_TIME")
        protected String accTime;

        @Column(name = "PERS_NR")
        protected Long persNr;

        @Column(name = "GF_CREATED_BRANCH")
        protected Long gfCreatedBranch;

        @Column(name = "FINANZIERUNGS_KZ")
        protected Long finanzierungsKz;

        @Column(name = "ABDECK_ENTHALTEN")
        protected Long abDeckEnthalten;

        @Column(name = "ABDECK_BETRAG")
        protected Long abDeckBetrag;

        @Column(name = "KREDITART")
        protected Double kreditArt;

        @Column(name = "PRODCODEPOL60")
        protected Double prodCodePol60;

        @Column(name = "GF_STATUS")
        protected Double gfStatus;

        @Column(name = "RATE")
        protected Double rate;

        @Column(name = "ROLLE")
        protected String rolle;

        @Column(name = "SACHBEARBEITER")
        protected Double sachbearbeiter;

        @Column(name = "WEBORDER_ID")
        protected String weborderId;

        public Long getAbDeckBetrag() {
                return abDeckBetrag;
        }

        public void setAbDeckBetrag(Long abDeckBetrag) {
                this.abDeckBetrag = abDeckBetrag;
        }

        public Long getAbDeckEnthalten() {
                return abDeckEnthalten;
        }

        public void setAbDeckEnthalten(Long abDeckEnthalten) {
                this.abDeckEnthalten = abDeckEnthalten;
        }

        public String getAccTime() {
                return accTime;
        }

        public void setAccTime(String accTime) {
                this.accTime = accTime;
        }

        public String getAltVtnr() {
                return altVtnr;
        }

        public void setAltVtnr(String altVtnr) {
                this.altVtnr = altVtnr;
        }

        public String getEntscheidungsKz() {
                return entscheidungsKz;
        }

        public void setEntscheidungsKz(String entscheidungsKz) {
                this.entscheidungsKz = entscheidungsKz;
        }

        public String getFinanzArt() {
                return finanzArt;
        }

        public void setFinanzArt(String finanzArt) {
                this.finanzArt = finanzArt;
        }

        public Long getFinanzierungsKz() {
                return finanzierungsKz;
        }

        public void setFinanzierungsKz(Long finanzierungsKz) {
                this.finanzierungsKz = finanzierungsKz;
        }

        public Long getGfCreatedBranch() {
                return gfCreatedBranch;
        }

        public void setGfCreatedBranch(Long gfCreatedBranch) {
                this.gfCreatedBranch = gfCreatedBranch;
        }

        public String getGfNummer() {
                return gfNummer;
        }

        public void setGfNummer(String gfNummer) {
                this.gfNummer = gfNummer;
        }

        public Double getGfStatus() {
                return gfStatus;
        }

        public void setGfStatus(Double gfStatus) {
                this.gfStatus = gfStatus;
        }

        public Long getGsKdnr() {
                return gsKdnr;
        }

        public void setGsKdnr(Long gsKdnr) {
                this.gsKdnr = gsKdnr;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public Long getKdnr() {
                return kdnr;
        }

        public void setKdnr(Long kdnr) {
                this.kdnr = kdnr;
        }

        public Double getKreditArt() {
                return kreditArt;
        }

        public void setKreditArt(Double kreditArt) {
                this.kreditArt = kreditArt;
        }

        public Double getKreditBetrag() {
                return kreditBetrag;
        }

        public void setKreditBetrag(Double kreditBetrag) {
                this.kreditBetrag = kreditBetrag;
        }

        public Long getKunde() {
                return kunde;
        }

        public void setKunde(Long kunde) {
                this.kunde = kunde;
        }

        public Long getLaufzeit() {
                return laufzeit;
        }

        public void setLaufzeit(Long laufzeit) {
                this.laufzeit = laufzeit;
        }

        public String getLvorname() {
                return lvorname;
        }

        public void setLvorname(String lvorname) {
                this.lvorname = lvorname;
        }

        public String getLzuname() {
                return lzuname;
        }

        public void setLzuname(String lzuname) {
                this.lzuname = lzuname;
        }

        public Long getPersNr() {
                return persNr;
        }

        public void setPersNr(Long persNr) {
                this.persNr = persNr;
        }

        public Double getProdCodePol60() {
                return prodCodePol60;
        }

        public void setProdCodePol60(Double prodCodePol60) {
                this.prodCodePol60 = prodCodePol60;
        }

        public Double getRate() {
                return rate;
        }

        public void setRate(Double rate) {
                this.rate = rate;
        }

        public String getRolle() {
                return rolle;
        }

        public void setRolle(String rolle) {
                this.rolle = rolle;
        }

        public Double getSachbearbeiter() {
                return sachbearbeiter;
        }

        public void setSachbearbeiter(Double sachbearbeiter) {
                this.sachbearbeiter = sachbearbeiter;
        }

        public String getSuchBegriff() {
                return suchBegriff;
        }

        public void setSuchBegriff(String suchBegriff) {
                this.suchBegriff = suchBegriff;
        }

        public String getVorname() {
                return vorname;
        }

        public void setVorname(String vorname) {
                this.vorname = vorname;
        }

        public String getWeborderId() {
                return weborderId;
        }

        public void setWeborderId(String weborderId) {
                this.weborderId = weborderId;
        }

        public String getZuname() {
                return zuname;
        }

        public void setZuname(String zuname) {
                this.zuname = zuname;
        }

        @Override
        public String toString() {
                final StringBuilder sb = new StringBuilder("CoreSearch{");
                sb.append("abDeckBetrag=").append(abDeckBetrag);
                sb.append(", abDeckEnthalten=").append(abDeckEnthalten);
                sb.append(", accTime='").append(accTime).append('\'');
                sb.append(", altVtnr='").append(altVtnr).append('\'');
                sb.append(", entscheidungsKz='").append(entscheidungsKz).append('\'');
                sb.append(", finanzArt='").append(finanzArt).append('\'');
                sb.append(", finanzierungsKz=").append(finanzierungsKz);
                sb.append(", gfCreatedBranch=").append(gfCreatedBranch);
                sb.append(", gfNummer='").append(gfNummer).append('\'');
                sb.append(", gfStatus=").append(gfStatus);
                sb.append(", gsKdnr=").append(gsKdnr);
                sb.append(", id=").append(id);
                sb.append(", kdnr=").append(kdnr);
                sb.append(", kreditArt=").append(kreditArt);
                sb.append(", kreditBetrag=").append(kreditBetrag);
                sb.append(", kunde=").append(kunde);
                sb.append(", laufzeit=").append(laufzeit);
                sb.append(", lvorname='").append(lvorname).append('\'');
                sb.append(", lzuname='").append(lzuname).append('\'');
                sb.append(", persNr=").append(persNr);
                sb.append(", prodCodePol60=").append(prodCodePol60);
                sb.append(", rate=").append(rate);
                sb.append(", rolle='").append(rolle).append('\'');
                sb.append(", sachbearbeiter=").append(sachbearbeiter);
                sb.append(", suchBegriff='").append(suchBegriff).append('\'');
                sb.append(", vorname='").append(vorname).append('\'');
                sb.append(", weborderId='").append(weborderId).append('\'');
                sb.append(", zuname='").append(zuname).append('\'');
                sb.append('}');
                return sb.toString();
        }
}