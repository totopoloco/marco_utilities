package at.mavila.jpa.pojos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by marcotulioavilaceron on 09/12/15.
 */
@Entity
@Table(name = "SAMPLE_USER")
public class User implements Serializable {

        private static final long serialVersionUID = 69607165849486868L;

        @Id
        @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq", allocationSize = 1)
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
        private Long userId;

        @Column(name = "name", nullable = false, length = 255)
        private String name;

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "addressId", nullable = false)
        private Address address;


        public User(){
                //Does nothing.
                super();
        }

        public User(Long id, String name, Address address) {
                this.userId = id;
                this.name = name;
                this.address = address;
        }

        public Long getUserId() {
                return this.userId;
        }

        public Address getAddress() {
                return this.address;
        }

        public String getName() {
                return this.name;
        }


        public void setAddress(Address address) {
                this.address = address;
                if(!address.getUsers().contains(this)) {
                        address.getUsers().add(this);
                }
        }

        public void setName(String name) {
                this.name = name;
        }

        public void setUserId(Long userId) {
                this.userId = userId;
        }

        @Override
        public boolean equals(final Object o) {
                return EqualsBuilder.reflectionEquals(o, this);
        }

        @Override
        public int hashCode() {
                return HashCodeBuilder.reflectionHashCode(this);
        }

        @Override
        public String toString() {

                final ReflectionToStringBuilder reflectionToStringBuilder = new ReflectionToStringBuilder(this,ToStringStyle.SHORT_PREFIX_STYLE );
                reflectionToStringBuilder.setExcludeFieldNames("address");
                return reflectionToStringBuilder.toString();


        }
}
