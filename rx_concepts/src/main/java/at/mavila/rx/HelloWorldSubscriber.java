package at.mavila.rx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Subscriber;
import rx.functions.Action1;

/**
 * Created by marcoavilac on 03/09/15.
 */
public class HelloWorldSubscriber implements Action1<String>{

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldSubscriber.class);

    public void call(String s) {
        LOGGER.info(s);
    }
//extends Subscriber<String> {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldSubscriber.class);
//
//    public void onCompleted() {
//        LOGGER.info("Completed.");
//    }
//
//    public void onError(Throwable e) {
//        LOGGER.warn("Error was thrown: {}",e.getMessage());
//    }
//
//    public void onNext(String s) {
//        LOGGER.info(s);
//    }
}
