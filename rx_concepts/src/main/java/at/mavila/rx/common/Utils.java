package at.mavila.rx.common;


import rx.Observable;
import rx.Subscriber;
import rx.observables.ConnectableObservable;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by 200000313 on 16.09.2015.
 */
public class Utils {

        public static ConnectableObservable<String> from(final BufferedReader reader) {
                return Observable.create(new LineReaderObservable(reader)).publish();
        }

        private static class LineReaderObservable implements Observable.OnSubscribe<String>{


                private final BufferedReader reader;

                public LineReaderObservable(BufferedReader reader) {
                        this.reader = reader;
                }

                @Override
                public void call(Subscriber<? super String> subscriber) {
                        if (subscriber.isUnsubscribed()) {
                                return;
                        }

                        try {
                                String line;
                                while (!subscriber.isUnsubscribed()
                                        && (line = this.reader.readLine()) != null) {
                                        if ("exit".equals(line) || line == null) {
                                                break;
                                        }
                                        subscriber.onNext(line);
                                }

                        } catch (IOException e) {
                                subscriber.onError(e);
                        }

                        if (!subscriber.isUnsubscribed()) {
                                subscriber.onCompleted();
                        }
                }
        }
}
