package at.mavila.rx.calculator;

import at.mavila.rx.common.ObservableFactory;
import rx.Observable;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * Created by 200000313 on 16.09.2015.
 */
public class NumberObservable implements ObservableFactory {

        private final String varName;
        private final Observable<String> input;
        private final Pattern pattern;

        public NumberObservable(final String varName, final Observable<String> input) {
                this.varName = varName;
                this.input = input;
                this.pattern = Pattern.compile("\\s*"+this.varName+"\\s*[:|=]\\s*(-?\\d+\\.?\\d*)$");
        }

//        @Override
//        public Observable<BigDecimal> getObservable() {
//                return this.input.map(new Func1<String, Matcher>() {
//                        @Override public Matcher call(String s) {
//                                return PATTERN.matcher(s);
//                        }
//                }).filter(new Func1<Matcher, Boolean>() {
//                        @Override public Boolean call(Matcher matcher) {
//                                return matcher.matches() && matcher.group(1) != null;
//                        }
//                }).map(new Func1<Matcher, BigDecimal>() {
//                        @Override public BigDecimal call(Matcher matcher) {
//                                return new BigDecimal(matcher.group(1));
//                        }
//                });
//        }



        @Override
        public Observable<BigDecimal> getObservable() {
                return this.input.map(s -> this.pattern.matcher(s)).filter(matcher -> matcher.matches() && matcher.group(1) != null).map(matcher1 -> new BigDecimal(matcher1.group(1)));
        }
}
