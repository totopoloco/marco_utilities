package at.mavila.rx.calculator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.Observer;
import rx.functions.Func2;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by 200000313 on 16.09.2015.
 */
public class ReactiveSumObserver implements Observer<BigDecimal> {

        private BigDecimal sum;
        private static final Logger LOGGER = LoggerFactory.getLogger(ReactiveSumObserver.class);

        public ReactiveSumObserver(Observable<BigDecimal> a, Observable<BigDecimal> b) {
                this.sum = new BigDecimal(0);

                Observable.combineLatest(a, b, new Func2<BigDecimal, BigDecimal, BigDecimal>() {
                        @Override public BigDecimal call(BigDecimal a, BigDecimal b) {
                                final BigDecimal added = a.add(b);
                                return added.setScale(2, RoundingMode.HALF_UP);

                        }
                }).subscribe(this);

                //Observable.combineLatest(a, b, (a, b) -> a.add(b)).subscribe(this);
        }

        @Override public void onCompleted() {
                LOGGER.info("Summed a + b = {}", this.sum.toString());
        }

        @Override public void onError(Throwable e) {
                LOGGER.error("Exception caught: {}", e);
        }

        @Override public void onNext(BigDecimal bigDecimal) {
                this.sum = bigDecimal;
                if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("update : a + b = {}", bigDecimal.toString());
                }
        }
}
