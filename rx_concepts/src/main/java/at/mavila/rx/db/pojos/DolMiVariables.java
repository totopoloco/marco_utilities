package at.mavila.rx.db.pojos;

/**
 * Created by 200000313 on 14.09.2015.
 */
public class DolMiVariables {
        private final String var;
        private final String var_value;
        private final String valid_from;
        private final String valid_until;
        private final String tarif;

        public DolMiVariables(String var, String var_value, String valid_from, String valid_until, String tarif) {
                this.var = var;
                this.var_value = var_value;
                this.valid_from = valid_from;
                this.valid_until = valid_until;
                this.tarif = tarif;
        }

        public String getVar() {
                return var;
        }

        public String getVar_value() {
                return var_value;
        }

        public String getValid_from() {
                return valid_from;
        }

        public String getValid_until() {
                return valid_until;
        }

        public String getTarif() {
                return tarif;
        }

        @Override public boolean equals(Object o) {
                if (this == o)
                        return true;
                if (!(o instanceof DolMiVariables))
                        return false;

                DolMiVariables that = (DolMiVariables) o;

                if (getVar() != null ? !getVar().equals(that.getVar()) : that.getVar() != null)
                        return false;
                if (getVar_value() != null ? !getVar_value().equals(that.getVar_value()) : that.getVar_value() != null)
                        return false;
                if (getValid_from() != null ?
                        !getValid_from().equals(that.getValid_from()) :
                        that.getValid_from() != null)
                        return false;
                if (getValid_until() != null ?
                        !getValid_until().equals(that.getValid_until()) :
                        that.getValid_until() != null)
                        return false;
                return !(getTarif() != null ? !getTarif().equals(that.getTarif()) : that.getTarif() != null);

        }

        @Override public int hashCode() {
                int result = getVar() != null ? getVar().hashCode() : 0;
                result = 31 * result + (getVar_value() != null ? getVar_value().hashCode() : 0);
                result = 31 * result + (getValid_from() != null ? getValid_from().hashCode() : 0);
                result = 31 * result + (getValid_until() != null ? getValid_until().hashCode() : 0);
                result = 31 * result + (getTarif() != null ? getTarif().hashCode() : 0);
                return result;
        }

        @Override public String toString() {
                final StringBuilder sb = new StringBuilder("DolMiVariables{");
                sb.append("var='").append(var).append('\'');
                sb.append(", var_value='").append(var_value).append('\'');
                sb.append(", valid_from='").append(valid_from).append('\'');
                sb.append(", valid_until='").append(valid_until).append('\'');
                sb.append(", tarif='").append(tarif).append('\'');
                sb.append('}');
                return sb.toString();
        }
}
