package at.mavila.rx.db.util;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by 200000313 on 14.09.2015.
 */
public interface ConnectionFactory {

        /**
         * Gets a db connection.
         *
         * @return a open Connection.
         * @throws ClassNotFoundException if the driver is not loaded in the classpath.
         * @throws SQLException if opening the connection did not succeed.
         */
        Connection getConnection() throws ClassNotFoundException, SQLException;

}
