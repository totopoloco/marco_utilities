package at.mavila.rx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

/**
 * Created by marcoavilac on 02/09/15.
 */
public class HelloWorldObservable {

    private Observable<String> observable;
    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldObservable.class);


    public HelloWorldObservable() {

//        this.observable = Observable.create(
//
//                new Observable.OnSubscribe<String>() {
//
//                    public void call(Subscriber<? super String> sub) {
//                        sub.onNext("Hello, world!");
//                        sub.onCompleted();
//                    }
//                }
//        );

        this.observable = this.observable.just("hello world!");

        LOGGER.info(this.observable.toString());
    }
    public Observable<String> getObservable() {
        return this.observable;
    }

}