package at.mavila.rx.db.pojos;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by marcoavilac on 9/13/2015.
 */
public class PersonTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonTest.class);

    @Before
    public void init(){
        BasicConfigurator.configure();
    }


    @Test
    public void test2() {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("test1");
        EntityManager theManager = factory.createEntityManager();
        theManager.getTransaction().begin();
        assertThat(theManager).isNotNull();

        Person person = new Person(null,"ana");
        //person.setFirstName("ana");
        theManager.persist(person);
        theManager.getTransaction().commit();
        LOGGER.info("Id: {}", person.getId());

        Person p = (Person) theManager.find(Person.class, 1);
        LOGGER.info("Id: {}", p.getId());


    }

}