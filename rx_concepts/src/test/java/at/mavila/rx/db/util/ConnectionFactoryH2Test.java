package at.mavila.rx.db.util;

import at.mavila.rx.db.pojos.Person;
import com.github.davidmoten.rx.jdbc.Database;
import com.github.davidmoten.rx.jdbc.tuple.TupleN;
import org.apache.log4j.BasicConfigurator;
import org.assertj.core.data.Index;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.functions.Action1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 200000313 on 14.09.2015.
 */
public class ConnectionFactoryH2Test {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionFactoryH2Test.class);

    @Before
    public void init() {
        BasicConfigurator.configure();
    }

    @Test
    public void testClassicJDBCCall() {

        ConnectionFactory connectionFactory = new ConnectionFactoryH2();
        List<Person> personList = new ArrayList<Person>();

        Statement stat = null;
        Connection connection = null;
        ResultSet rs = null;
        try {
            connection = connectionFactory.getConnection();
            stat = createSampleData(connection);
                        /*Query the created data*/
            rs = stat.executeQuery("select id,firstName from test");
                        /*We are familiar with this, don't we?*/
            while (rs.next()) {
                Person person = new Person(rs.getInt("id"), rs.getString("firstName"));
                LOGGER.info(person.toString());
                personList.add(person);
            }
                        /*End while*/

        } catch (Exception exception) {
            LOGGER.error("Boom!, Exception caught: {}", exception);
        } finally {
            if (stat != null) {
                try {
                    stat.close();
                } catch (SQLException e) {
                    LOGGER.error("Boom!, SQLException caught: {}", e);
                }
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    LOGGER.error("Boom!, SQLException caught: {}", e);
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Boom!, SQLException caught: {}", e);
                }
            }
        }

        assertThat(personList).isNotEmpty();
        assertThat(personList).contains(new Person(1, "Hola"), Index.atIndex(0));
        assertThat(personList).contains(new Person(7, "mal"), Index.atIndex(6));
    }


    @Test
    public void testRxJavaJDBCCall() throws SQLException, ClassNotFoundException {

                /*Boiler plate code, just a sample to create some data*/
        ConnectionFactory connectionFactory = new ConnectionFactoryH2();
        final Connection connection = connectionFactory.getConnection();
        final Statement statement = createSampleData(connection);

        Database database = Database.from(connection);

                /*Example 1.
                 - Simple case.
                */
        final String sqlSingle = "select id,firstName from test";
        List<Person> personList = database.select(sqlSingle).autoMap(Person.class).toList().toBlocking().single();

        assertThat(personList).isNotEmpty();
        assertThat(personList).contains(new Person(1, "Hola"), Index.atIndex(0));
        assertThat(personList).contains(new Person(7, "mal"), Index.atIndex(6));


                /*Example 2.
                - Execute a new example with parameters*/
        final String sql = "select id,firstName from test where id >= :min and id <= :max order by id";
        List<Person> personList1 = database.select(sql).parameter("min", 2).parameter("max", 4).autoMap(Person.class).toList().toBlocking().single();
        assertThat(personList1).hasSize(3);
        assertThat(personList1).contains(new Person(2, "como"), Index.atIndex(0));

        final String sql_To_Char = "select to_char(id),firstName from test where id >= :min and id <= :max order by id";
        TupleN<String> tuple2 = database.select(sql_To_Char).parameter("min", 2).parameter("max", 4).getTupleN(String.class).first().toBlocking().single();

        final String tuples2Value0 = tuple2.values().get(0);
        final String tuples2Value1 = tuple2.values().get(1);

        statement.close();
        connection.close();
    }

    @Test
    public void testWithObservable() throws SQLException, ClassNotFoundException {
                /*Boiler plate code, just a sample to create some data*/
        ConnectionFactory connectionFactory = new ConnectionFactoryH2();
        final Connection connection = connectionFactory.getConnection();
        final Statement statement = createSampleData(connection);

        Database database = Database.from(connection);


                  /*Example 1.
                 - Simple case.
                */
        final String sqlSingle = "select id,firstName from test";
        Observable<Person> personObservable = database.select(sqlSingle).autoMap(Person.class);


        ///final Observable<Person> last = personObservable.last();
        //final Person last1 = personObservable.toBlocking().last();

        personObservable.countLong().subscribe(new Action1<Long>() {
            @Override
            public void call(Long aLong) {
                LOGGER.info(aLong.toString());
            }
        });

        personObservable.contains(new Person(2,"ok")).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                LOGGER.info(aBoolean.toString());
            }
        });

        // assertThat(last1.getId()).isEqualTo(7);

        //Iterable<Person> personIterable = personObservable.toBlocking().toIterable();

        // assertThat(personIterable).isNotEmpty();
        // assertThat(personIterable).contains(new Person(1, "Hola"));
        //assertThat(personIterable).contains(new Person(7, "mal"));

        database.close();
    }


    private void createData(Statement stat) throws SQLException {
                /*Create a table*/
        stat.execute("create table test(id int primary key, firstName varchar(255))");
                /*Insert some data on the newly created table*/
        stat.execute("insert into test values(1, 'Hola')");
        stat.execute("insert into test values(2, 'como')");
        stat.execute("insert into test values(3, 'estas')");
        stat.execute("insert into test values(4, 'espero')");
        stat.execute("insert into test values(5, 'no del todo')");
        stat.execute("insert into test values(6, 'muy')");
        stat.execute("insert into test values(7, 'mal')");
    }


    private Statement createSampleData(Connection connection) {
                 /*Create sample data*/
        Statement stat = null;
        try {
            stat = connection.createStatement();
            createData(stat);
        } catch (Exception exception) {
            LOGGER.error("Boom!, Exception caught: {}", exception);
        }
        return stat;

    }

}