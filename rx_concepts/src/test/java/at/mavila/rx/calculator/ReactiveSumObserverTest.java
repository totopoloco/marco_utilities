package at.mavila.rx.calculator;

import at.mavila.rx.common.ObservableFactory;
import at.mavila.rx.common.Utils;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.Subscription;
import rx.observables.ConnectableObservable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by 200000313 on 16.09.2015.
 */
public class ReactiveSumObserverTest {


        private static final Logger LOGGER = LoggerFactory.getLogger(ReactiveSumObserverTest.class);

        @Before
        public void init(){
                BasicConfigurator.configure();
        }


        @Test
        public void testSimpleCase(){

                ConnectableObservable<String> input = Utils.from(new BufferedReader(new InputStreamReader(System.in)));


                ObservableFactory<BigDecimal> a = new NumberObservable("a",input);
                ObservableFactory<BigDecimal> b = new NumberObservable("b",input);

                ReactiveSumObserver sumObserver = new ReactiveSumObserver(a.getObservable(),b.getObservable());

                final Subscription subscription = input.connect();
                final boolean unsubscribed = subscription.isUnsubscribed();



        }


        public static void main(String[] args){
                BasicConfigurator.configure();
                ReactiveSumObserverTest sumObserverTest = new ReactiveSumObserverTest();
                sumObserverTest.testSimpleCase();
        }

}